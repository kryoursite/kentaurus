<?php

/**
 * Simple page header block
 *
 * @package Kentaurus
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

// Get_field variables

$sportsbookField = get_field('sportsbook_affiliate_link');
$casinoField = get_field('casino_redirect_link');

?>

<div class="casino-list2-item2 base-style2" <?php if (get_field('enable_terms_and_conditions')) {
                                                echo 'style="_toplist-base2.scss"';
                                            } else {
                                                echo 'style="margin-bottom:50px;"';
                                            } ?>>
    <span class="casino-list2__order2"></span>
    <div class="casino-list2__logo2">
    <?php if (!$casinoField) : ?> 
        <?php the_post_thumbnail(); ?>
    <?php else : ?>
        <a href="<?php the_field('casino_redirect_link'); ?>">
            <?php the_post_thumbnail(); ?>
        </a>
    <?php endif; ?>
    </div>
    <div class="casino-list2__bonus2">
        <?php if ($atts['type'] == 'sportsbook') : ?>
            <?php the_field('casino_sportsbook_line'); ?>
        <?php else : ?>
            <?php the_field('casino_toplist_bonus_line'); ?>
        <?php endif; ?>
    </div>
    <!-- <div class="casino-list2__rating2">
        <?php if ($atts['type'] == 'sportsbook') : ?>
            <span class="casino-name2"><?php the_field('casino_extra_rating_text'); ?></span>
        <?php else : ?>
            <span class="casino-name2"><?php the_title(); ?></span>
        <?php endif; ?>
        <div class="star-rating2"><i class="star"></i></div>
        <div class="casino-rating2">
            <?php if (get_field('casino_toplist_rating')) : ?>
                <?php the_field('casino_toplist_rating'); ?>
            <?php else : ?>
                <?php echo '-'; ?>
            <?php endif; ?>

        </div>
    </div> -->
    <!-- <div class="casino-list2__pluses2">
        <?php if ($atts['type'] == 'sportsbook') : ?>
            <?php
            if (have_rows('sportsbook_top_3')) :
                while (have_rows('sportsbook_top_3')) : the_row(); ?>
                    <span class="plus2"><i class="fas fa-check"></i><?php the_sub_field('top_three_line'); ?></span>
            <?php
                endwhile;
            endif;
            ?>
        <?php else : ?>
            <?php
            if (have_rows('casino_top_3')) :
                while (have_rows('casino_top_3')) : the_row(); ?>
                    <span class="plus2"><i><span class="checkmark">
                            <div class="checkmark_stem"></div>
                            <div class="checkmark_kick"></div>
                    </span></i><?php the_sub_field('top_three_line'); ?></span>
            <?php
                endwhile;
            endif;
            ?>
        <?php endif; ?>
    </div> -->
    <div class="casino-list2__more2">
        <a href="<?php the_permalink(); ?>" class="list-review2"><?php _e('Casino Review', 'kentaurus'); ?> <i class="arrow arrow-right"></i></a>
        <?php if ($atts['type'] == 'sportsbook') : ?>
            <?php if ($casinoField) : ?>
            <a href="<?php the_field('sportsbook_affiliate_link'); ?> " rel="nofollow noopener" target="_blank"  class="to-the-casino2"><?php _e('Get Bonus!', 'kentaurus'); ?></a>
            <?php endif; ?>
        <?php elseif ($atts['type'] == 'casino') : ?>
            <?php if ($casinoField) : ?>
            <a href="<?php the_field('casino_redirect_link'); ?>" rel="nofollow noopener" target="_blank" class="to-the-casino2"><?php _e('Get Bonus!', 'kentaurus'); ?></a>
            <?php endif; ?>
        <?php endif; ?>
    </div>
    <?php if (get_field('enable_terms_and_conditions')) : ?>
        <div class="casino-list2__terms2">
        <img class="info-icon-toplist" width="10" height="10" src="<?php echo KENTAURUS_DIR_URI . '/dist/img/logos/info.png' ?>" alt="info-logo"></img><span><?php the_field('terms_and_conditions'); ?></span>
        </div>
    <?php endif; ?>
</div>