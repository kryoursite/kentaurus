<?php

/**
 * Casino archive page header
 *
 * @package kentaurus
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>

<div class="page-title">
    <?php if (get_field('casino_archive_page_title', 'options')) : ?>
        <h1>
            <?php the_field('casino_archive_page_title', 'options'); ?>
        </h1>
    <?php else : ?>
        <h1>Casino Archive Page</h1>
    <?php endif; ?>
</div>
<div class="page-information">
    <div class="breadcrumbs">
        <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
    </div>
    <div class="info-separator mx-10"><i class="none">||</i></div>
    <div class="last-update">
        <span><?php _e('Updated: ', 'Kentaurus'); ?><?php the_modified_date("d/m/Y"); ?></span>
    </div>
</div>
<?php if (get_field('casino_archive_page_introduction', 'options')) : ?>
    <p>
        <?php the_field('casino_archive_page_introduction', 'options'); ?>
    </p>
<?php endif; ?>