<?php

/**
 * Footer Template
 * 
 */

?>
<footer id="footer" class="footer">
    <div class="inner-footer container py-20">
        <div class="footer-upper">
            <div class="footer-menus">
                <?php if (has_nav_menu("footer-menu-1")) : ?>
                    <?php echo kentaurus_generate_footer_menu("footer-menu-1"); ?>
                <?php endif; ?>
                <?php if (has_nav_menu("footer-menu-2")) : ?>
                    <?php echo kentaurus_generate_footer_menu("footer-menu-2"); ?>
                <?php endif; ?>
                <?php if (has_nav_menu("footer-menu-3")) : ?>
                    <?php echo kentaurus_generate_footer_menu("footer-menu-3"); ?>
                <?php endif; ?>
                <?php if (has_nav_menu("footer-menu-4")) : ?>
                    <?php echo kentaurus_generate_footer_menu("footer-menu-4"); ?>
                <?php endif; ?>
                <?php if (has_nav_menu("footer-menu-5")) : ?>
                    <?php echo kentaurus_generate_footer_menu("footer-menu-5"); ?>
                <?php endif; ?>
            </div>
            <div class="footer-logos my-20">

                <?php if (have_rows('footer_logos', 'options')) : ?>

                    <?php while (have_rows('footer_logos', 'options')) : the_row();
                        $image = get_sub_field('footer_logo', 'options');
                        $img_link = get_sub_field('footer_enable_img_link', 'options');
                        $link = get_sub_field('footer_logo_link', 'options');
                    ?>
                        <?php if ($img_link) : ?>
                            <a href="<?php echo $link; ?>" class="footer-logo-wrap">
                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                            </a>
                        <?php else : ?>
                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                        <?php endif; ?>
                    <?php endwhile; ?>

                <?php endif; ?>
            </div>
        </div>
        <div class="footer-lower">
            <?php if (get_field('footer_info_text', 'options')) : ?>
                <?php the_field('footer_info_text', 'options'); ?>

         <div class="footer-custom">   
            <!-- <div class="footer-lower__language">
            <img class="globe-icon" height="24" width="24" src="<?php //echo KENTAURUS_DIR_URI . '/dist/img/logos/globe.png' ?>" alt="globe-logo"></img>
                <?php //do_action('wpml_add_language_selector');  ?>  
            </div> -->
            <div class="footer-logo">
                <?php 
                    $footer_logo_url = get_theme_mod( 'footer_site_logo' );
                        if ($footer_logo_url )
                    echo '<img alt="footer-logo" src="'.$footer_logo_url.'">';
                ?>            
            </div> 
        </div>
           
           
         
            <?php endif; ?>
            <div class="footer-socials">
                <?php if (get_field('social_twitter', 'options')) : ?>
                    <a href="<?php echo get_field('social_twitter', 'options'); ?>"><img class="twitter-icon" height="24" width="24" src="<?php echo KENTAURUS_DIR_URI . '/dist/img/logos/twitter.png' ?>" alt="twitter-logo"></img></a>
                <?php endif; ?>
                <?php if (get_field('social_facebook', 'options')) : ?>
                    <a href="<?php echo get_field('social_instagram', 'options'); ?>"><img class="inst-icon" height="24" width="24" src="<?php echo KENTAURUS_DIR_URI . '/dist/img/logos/inst.png' ?>" alt="inst-logo"></img></a>
                <?php endif; ?>
                <?php if (get_field('social_instagram', 'options')) : ?>
                    <a href="<?php echo get_field('social_facebook', 'options'); ?>"><img class="fb" height="24" width="24" src="<?php echo KENTAURUS_DIR_URI . '/dist/img/logos/fb.png' ?>" alt="fb-logo"></img></i></a>
                <?php endif; ?>
            </div>
        </div>
        
    </div>
    <div class="footer-copyright">
        <div class="inner-copyright container py-10">
            <span class="footer-date">
                <?php echo date("Y") . " "; ?><?php _e("© All Rights Reserved", 'kentaurus'); ?>
            </span>
        </div>
    </div>
</footer>
</div> <!-- .wrapper end -->
<?php wp_footer(); ?>
</body>

</html>