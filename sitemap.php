<?php

/**
 * Template Name: Sitemap Template
 */
get_header(); ?>



<div class = "sitemap-body">    

<div class = "body-row  container">
	<h2 class="sitemap-title" id="pages"><?php _e('Pages', 'kentaurus'); ?></h2>
	<ul>
	<?php
	// Add pages you'd like to exclude in the exclude here
	wp_list_pages(
	  array(
	    'exclude' => '',
	    'title_li' => '',
		'depth' => 1,	
	  )
	);
	?>
	</ul>
</div>

<div class = "body-row  container">
<h2 class="sitemap-title" id="sitemap-our-work"><?php _e('Casino', 'kentaurus'); ?></h2>
<ul>
<?php
$postsArgs = array(
	'post_type' => 'casino',
	'posts_per_page'=>'-1',
	//'post__not_in' => array(),
);
$postsLoop = new WP_Query( $postsArgs );
while ( $postsLoop->have_posts() ) {
	$postsLoop->the_post();
?>
	<li <?php post_class(); ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
<?php } wp_reset_query(); ?>
</ul>
</div>

<div class = "body-row  container">
<h2 class="sitemap-title" id="sitemap-our-work"><?php _e('Casino Slots', 'kentaurus'); ?></h2>
<ul>
<?php
$postsArgs = array(
	'post_type' => 'casino-slot',
	'posts_per_page'=>'-1',
	//'post__not_in' => array(),
);
$postsLoop = new WP_Query( $postsArgs );
while ( $postsLoop->have_posts() ) {
	$postsLoop->the_post();
?>
	<li <?php post_class(); ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
<?php } wp_reset_query(); ?>
</ul>
</div>


<div class = "body-row  container">
	<h2 class="sitemap-title" id="XML"><?php _e('XML Sitemap', 'kentaurus'); ?></h2>
    <ul>
    	<li>
        <a class="sitemap-xml" href = "https://www.casinobaltics.com/sitemap_index.xml">https://www.casinobaltics.com/sitemap_index.xml</a>
    	</li>
    </ul>
</div>
</div>

<?php get_footer(); ?>